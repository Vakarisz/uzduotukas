<?php

namespace AppBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Table(name="users")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 * @UniqueEntity("username")
 */
class User implements UserInterface, \Serializable
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=25, unique=true)
     * @Assert\Length(min = 3,
     *      max = 50,
     *      minMessage = "Your username must be at least {{ limit }} characters long!",
     *      maxMessage = "Your username cannot be longer than {{ limit }} characters!"
     * )
     */
    private $username;
    /**
     * @ORM\Column(type="string", length=25, unique=false)
     * @Assert\NotBlank()
     * @Assert\Length(min = 3,
     *      max = 50,
     *      minMessage = "Your First name must be at least {{ limit }} characters long",
     *      maxMessage = "Your First name cannot be longer than {{ limit }} characters"
     * )
     */
    private $firstname;
    /**
     * @ORM\Column(type="string", length=25, unique=false)
     * @Assert\Length(min = 3,
     *      max = 50,
     *      minMessage = "Your Last name must be at least {{ limit }} characters long",
     *      maxMessage = "Your Last name cannot be longer than {{ limit }} characters"
     * )
     */
    private $secondname;

    /**
     * @ORM\Column(type="string", length=64)
     * @Assert\Length(min = 7,
     *      max = 64,
     *      minMessage = "Your password myst be at least {{ limit }} characters long",
     *      maxMessage = "Your password be longer than {{ limit }} characters"
     * )
     */
    private $password;


    public function __construct()
    {
        $this->isActive = true;
        // may not be needed, see section on salt below
        // $this->salt = md5(uniqid(null, true));
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function getSalt()
    {
        // you *may* need a real salt depending on your encoder
        // see section on salt below
        return null;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function getRoles()
    {
        return array('ROLE_USER');
    }

    public function eraseCredentials()
    {
    }

    /** @see \Serializable::serialize() */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->username,
            $this->password,
            // see section on salt below
            // $this->salt,
        ));
    }

    /** @see \Serializable::unserialize() */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->username,
            $this->password,
            // see section on salt below
            // $this->salt
        ) = unserialize($serialized);
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set username
     *
     * @param string $username
     *
     * @return User
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }
    /**
     * Set firstname
     *
     * @param string $firstname
     *
     * @return User
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname
     *
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set secondname
     *
     * @param string $secondname
     *
     * @return User
     */
    public function setSecondname($secondname)
    {
        $this->secondname = $secondname;

        return $this;
    }

    /**
     * Get secondname
     *
     * @return string
     */
    public function getSecondname()
    {
        return $this->secondname;
    }
}
