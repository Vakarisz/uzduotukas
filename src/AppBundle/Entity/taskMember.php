<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * taskMember
 *
 * @ORM\Table(name="task_members")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\taskMemberRepository")
 */
class taskMember
{
    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="task_members")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $task_user;

    /**
     * @ORM\ManyToOne(targetEntity="Task", inversedBy="task_members")
     * @ORM\JoinColumn(name="task_id", referencedColumnName="id")
     */
    protected $task;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=40)
     */
    private $name;

    /**
     * @var int
     *
     * @ORM\Column(name="task_id", type="integer")
     */
    private $taskId;

    /**
     * @var int
     *
     * @ORM\Column(name="user_id", type="integer")
     */
    private $userId;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return taskMember
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set taskId
     *
     * @param integer $taskId
     *
     * @return taskMember
     */
    public function setTaskId($taskId)
    {
        $this->taskId = $taskId;

        return $this;
    }

    /**
     * Get taskId
     *
     * @return int
     */
    public function getTaskId()
    {
        return $this->taskId;
    }

    /**
     * Set task
     *
     * @param Task $task
     *
     * @return task
     */
    public function setTask(Task $task){
        $this->task = $task;
        return $task;
    }

    /**
     * Set user
     *
     * @param User $user
     *
     * @return user
     */
    public function setUser(User $user){
        $this->task_user = $user;
        return $user;
    }

    /**
     * Get user
     *
     * @return user
     */
    public function getUser(){
        return $this->task_user;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return taskMember
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return int
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set taskUser
     *
     * @param \AppBundle\Entity\User $taskUser
     *
     * @return taskMember
     */
    public function setTaskUser(\AppBundle\Entity\User $taskUser = null)
    {
        $this->task_user = $taskUser;

        return $this;
    }

    /**
     * Get taskUser
     *
     * @return \AppBundle\Entity\User
     */
    public function getTaskUser()
    {
        return $this->task_user;
    }

    /**
     * Get task
     *
     * @return \AppBundle\Entity\Task
     */
    public function getTask()
    {
        return $this->task;
    }
}
