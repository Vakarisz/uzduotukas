<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Admins
 *
 * @ORM\Table(name="admins")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AdminsRepository")
 */
class Admins
{
    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="admins")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $project_admin;

    /**
     * @ORM\ManyToOne(targetEntity="Project", inversedBy="project")
     * @ORM\JoinColumn(name="project_id", referencedColumnName="id")
     */
    protected $project;


    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="project_id", type="integer")
     */
    private $projectId;

    /**
     * @var int
     *
     * @ORM\Column(name="user_id", type="integer")
     */
    private $userId;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set projectId
     *
     * @param integer $projectId
     *
     * @return Admins
     */
    public function setProjectId($projectId)
    {
        $this->projectId = $projectId;

        return $this;
    }

    /**
     * Get projectId
     *
     * @return int
     */
    public function getProjectId()
    {
        return $this->projectId;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return Admins
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return int
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set projectAdmin
     *
     * @param \AppBundle\Entity\User $projectAdmin
     *
     * @return Admins
     */
    public function setProjectAdmin(\AppBundle\Entity\User $projectAdmin = null)
    {
        $this->project_admin = $projectAdmin;

        return $this;
    }

    /**
     * Get projectAdmin
     *
     * @return \AppBundle\Entity\User
     */
    public function getProjectAdmin()
    {
        return $this->project_admin;
    }

    /**
     * Set project
     *
     * @param \AppBundle\Entity\Project $project
     *
     * @return Admins
     */
    public function setProject(\AppBundle\Entity\Project $project = null)
    {
        $this->project = $project;

        return $this;
    }

    /**
     * Get project
     *
     * @return \AppBundle\Entity\Project
     */
    public function getProject()
    {
        return $this->project;
    }
}
