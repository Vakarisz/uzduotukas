<?php

namespace AppBundle\Form;

use AppBundle\Entity\Task;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\ChoiceList\Loader\CallbackChoiceLoader;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Join;

class TaskType extends AbstractType
{
    static $id;
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, array('attr' => array('class' => 'form-control', 'style' => 'margin-bottom:15px')))
            ->add('description', TextareaType::class, array('attr' => array('class' => 'form-control', 'style' => 'margin-bottom:15px')))
            ->add('deadline', DateTimeType::class, array('attr' => array('class' => 'formcontrol', 'style' => 'margin-bottom:15px')))
            ->add('status', ChoiceType::class, array(
                'attr' => array('class' => 'form-control', 'style' => 'margin-bottom:15px'),
                'choices' => array(
                    'Idle' => 'idle',
                    'Active' => 'active',
                    'Finished' => 'finished',
                    'Accepted' => 'accepted'
                )))
            ->add('project', EntityType::class, array(
                'attr' => array('class' => 'form-control', 'style' => 'margin-bottom:15px'),
                'class' => 'AppBundle:Project',
                'query_builder' => function (EntityRepository $er) use ( $options ) {
                    $er = $er->createQueryBuilder('u')
                        ->orderBy('u.name', 'ASC');
                    $er->innerJoin(
                        'AppBundle:Member',
                        'c',
                        Join::WITH,
                        $er->expr()->eq('u.id', 'c.projectId')
                    );
                    $er->innerJoin(
                        'AppBundle:User',
                        'q',
                        Join::WITH,
                        $er->expr()->eq($options['userId'], 'c.userId')
                    );
                    $er->distinct();
                return $er;
                },
                'choice_label' => 'name',
            ))
            ->add('save', SubmitType::class, array('label' => 'Save'));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Task::class,
            'userId'     => null
        ));
    }
}