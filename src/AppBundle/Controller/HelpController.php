<?php

namespace AppBundle\Controller;

use AppBundle\Entity\taskMember;
use AppBundle\Entity\Task;
use AppBundle\Form\UserType;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Project;
use AppBundle\Entity\Members;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\User;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use AppBundle\Form\TaskType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;


class HelpController extends Controller
{

    /**
     * @Route("/help", name="help")
     */
    public function homeAction(Request $request)
    {
        return $this->render('main/help.html.twig',array());
    }
}
