<?php

namespace AppBundle\Controller;

use AppBundle\Entity\taskMember;
use AppBundle\Entity\Task;
use AppBundle\Entity\User;
use AppBundle\Form\UserType;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Project;
use AppBundle\Entity\Members;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use AppBundle\Form\RegistrationType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

class DefaultController extends Controller
{
    /**
     * @Route("/help", name="help")
     */
    public function helpAction(Request $request)
    {
        return $this->render('main/help.html.twig',array());
    }

    /**
     * @Route("/profile", name="profile")
     */
    public function profileAction(Request $request)
    {
        $user = $this->getUser()->getid();

        // can't properly transmit user
        return $this->render('main/profile.html.twig',array( 'user' => $user));
    }

    /**
     * @Route("/profile/edit/{id}", name="edit_profile")
     */
    public function editProfile(User $user, Request $request)
    {
       // $user = new User();
        $form = $this->createForm(RegistrationType::class,$user,array('userId' => $this->getUser()->getId()));

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {

            $firstName = $form['firstName']->getData();
            $secondname = $form['secondname']->getData();
            $password = $form['password']->getData();

            $em = $this->getDoctrine()->getManager();

            $user->setFirstname($user->getFirstname());
            $user->setSecondname($user->getSecondname());
            $user->setPassword($user->getPassword());

            $em->flush();

            $this->addFlash(
                'success',
                'Profile Edited'
            );
            return $this->redirectToRoute('profile');
        }
        return $this->render('main/profileEdit.html.twig',array(
            'user' => $user,
            'form'    => $form->createView()
        ));
    }
}
