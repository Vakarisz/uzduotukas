<?php

namespace AppBundle\Controller;

use AppBundle\Entity\taskMember;
use AppBundle\Entity\Task;
use AppBundle\Form\UserType;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Project;
use AppBundle\Entity\Members;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\User;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use AppBundle\Form\TaskType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

class TaskController extends Controller
{
    /**
     * @Route("/tasks", name="taskList")
     */
    public function homeAction(Request $request)
    {
        $userID = $this->getUser()->getid();
        $memberOf = $this->getDoctrine()->getRepository('AppBundle:Member')->findBy(
            array('userId' => $userID)
        );
        $projects = array();
        $index = 0;
        foreach($memberOf as $member) {
            $projects[$index] = $member->getProject();
            $index++;
        }
        $memberOf = $this->getDoctrine()->getRepository('AppBundle:Admins')->findBy(
            array('userId' => $userID)
        );
        $index = 0;
        foreach($memberOf as $member) {
            $projects[$index] = $member->getProject();
            $index++;
        }

        $userID = $this->getUser()->getid();
        $memberOf = $this->getDoctrine()->getRepository('AppBundle:taskMember')->findBy(
            array('userId' => $userID)
        );
        $tasks = array();
        $index = 0;
        foreach($memberOf as $member) {
            $tasks[$index] = $member->getTask();
            $index++;
        }
        $freeTasks = $this->getDoctrine()->getRepository('AppBundle:Task')->findAll();
        foreach($tasks as $task)
            $freeTasks = $this->unsetValue($freeTasks, $task);

        foreach($freeTasks as $task){
          if(!in_array($task->getProject(),$projects))
          {
              $freeTasks = $this->unsetValue($freeTasks, $task);
          }
        }

        return $this->render('main/taskList.html.twig',array(
            'tasks'     => $tasks,
            'freeTasks' => $freeTasks,
        ));
    }

    /**
     * @Route("/task/create/", name="createTask")
     */
    public function createTask(Request $request)
    {
        $task = new Task();
        $task_form = $this->createForm(TaskType::class,$task,array('userId' => $this->getUser()->getId()));
        $task_form->handleRequest($request);

        if ($task_form->isSubmitted() && $task_form->isValid()) {

            $name = $task_form['name']->getData();
            $description = $task_form['description']->getData();
            $deadline = $task_form['deadline']->getData();
            $status = $task_form['status']->getData();
            $project = $task_form['project']->getData();

            $task->setName($name);
            $task->setDescription($description);
            $task->setDeadline($deadline);
            $task->setStatus($status);
            $task->setTaskOwner($this->getUser()->getid());
            $task->setIsActive(true);
            $task->setProject($project);

            $em = $this->getDoctrine()->getManager();
            $em->persist($task);
            $em->flush();

            $thisTask = $this->getDoctrine()->getRepository('AppBundle:Task')->findBy(
                array('name' => $name)
            );
            $member = new taskMember();
            $member->setTask($thisTask[0]);
            $member->setName($this->getUser()->getusername());
            $member->setUser($this->getUser());

            $em2 = $this->getDoctrine()->getManager();
            $em2->persist($member);
            $em2->flush();

            $this->addFlash(
                'success',
                'Task successfully created!'
            );

            return $this->redirectToRoute("taskList");

        }

        return $this->render('main/createTask.html.twig',array(
            'task' => $task_form->createView()
        ));

    }

    /**
     * @Route("/task/details/{id}", name="task_details")
     */
    public function indexAction(Task $task)
    {
        $tasks = self::getUserTasks($this->getUser()->getId());

        if(in_array($task,$tasks,true)) {

            $members = $task->getTaskMembers();
            return $this->render('main/taskDetails.html.twig', array(
                    'task' => $task,
                    'members' => $members,
                    'project' => $task->getProject()->getName()
                )
            );
        }else{
            $this->addFlash(
                'danger',
                'You dont have that task assigned.'
            );
            return $this->redirectToRoute('taskList');

        }
    }

    /**
     * @Route("/task/edit/{id}", name="task_edit")
     */
    public function editAction(Task $task,Request $request)
    {

        $form = $this->createForm(TaskType::class,$task,array('userId' => $this->getUser()->getId()));
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {

            $name = $form['name']->getData();
            $description = $form['description']->getData();
            $deadline = $form['deadline']->getData();
            $status = $form['status']->getData();

            $em = $this->getDoctrine()->getManager();

            $task->setName($task->getName());
            $task->setDescription($task->getDescription());
            $task->setDeadline($task->getDeadline());
            $task->setStatus($task->getStatus());

            $em->flush();

            $this->addFlash(
                'success',
                'Task Edited'
            );
            return $this->redirectToRoute('taskList');
        }
        return $this->render('main/edit.html.twig',array(
            'task' => $task,
            'form'    => $form->createView()
        ));
    }

    /**
     * @Route("/task/quit/{id}", name="quitTask")
     */
    public function quitAction(Task $task)
    {
        $em = $this->getDoctrine()->getManager();
        $taskMember = $em->getRepository('AppBundle:taskMember')
            ->findBy(array(
                'userId' => $this->getUser()->getId(),
                'taskId' => $task->getId()
            ));
        $em->remove($taskMember[0]);
        $em->flush();

        $this->addFlash(
            'success',
            'Task Quited'
        );
        return $this->redirectToRoute("taskList");
    }
    /**
     * @Route("/task/pick/{id}", name="pickTask")
     */
    public function pickAction(Task $task)
    {
        $taskMember = new taskMember();
        $user = $this->getUser();
        $taskMember->setName($user->getusername());
        $taskMember->setUser($user);
        $taskMember->setTask($task);

        $em = $this->getDoctrine()->getManager();
        $em->persist($taskMember);
        $em->flush();

        $this->addFlash(
            'success',
            'Task Picked'
        );
        return $this->redirectToRoute("taskList");
    }
    /**
     * @Route("/task/delete/{id}", name="deleteTask")
     */
    public function deleteAction(Task $task, Request $request)
    {
        $form = $this->createFormBuilder()
            ->add('Password',passwordType::class)
            ->add('Delete',SubmitType::class)
            ->getForm();
        $form->handleRequest($request);

        if($task->getTaskowner() == $this->getUser()->getId()){

            if($form->isSubmitted() && $form->isValid()) {
                $password = $form['Password']->getdata();

                $factory = $this->get('security.encoder_factory');
                $encoder = $factory->getEncoder($this->getUser());

                if ($encoder->isPasswordValid($this->getUser()->getPassword(), $password, $this->getUser()->getSalt())) {
                    $task->setIsActive(0);

                    $em = $this->getDoctrine()->getManager();
                    $members = $this->getDoctrine()->getRepository('AppBundle:taskMember')->findBy(array(
                        'taskId' => $task ->getId()
                    ));

                    $em = $this->getDoctrine()->getManager();
                    foreach($members as $member)
                        $em->remove($member);
                    $em->remove($task);
                    $em->flush();

                    $this->addFlash(
                        'success',
                        'Task Deleted'
                    );

                    return $this->redirectToRoute("taskList");
                } else {
                    $this->addFlash(
                        'danger',
                        'Incorrect Password'
                    );
                    return $this->redirectToRoute("taskList");
                }
            }
            return $this->render('main/quit.html.twig',array(
                'form' => $form->createView()
            ));

        }else{
            $this->addFlash(
                'danger',
                'You are not task owner'
            );
            return $this->redirectToRoute("taskList");
        }

    }
    private function unsetValue(array $array, $value, $strict = TRUE)
    {
        if (($key = array_search($value, $array, $strict)) !== FALSE) {
            unset($array[$key]);
        }
        return $array;
    }
    /*
     * Get all user tasks. IMPORTANT! Differend from taskList.
     */
    private function getUserTasks($userID){

        $memberOf = $this->getDoctrine()->getRepository('AppBundle:Member')->findBy(
            array('userId' => $userID)
        );
        $projects = array();
        $index = 0;
        foreach($memberOf as $member) {
            $projects[$index] = $member->getProject();
            $index++;
        }
        $memberOf = $this->getDoctrine()->getRepository('AppBundle:Admins')->findBy(
            array('userId' => $userID)
        );
        $index = 0;
        foreach($memberOf as $member) {
            $projects[$index] = $member->getProject();
            $index++;
        }

        $allTasks = $this->getDoctrine()->getRepository('AppBundle:Task')->findAll();
        foreach($allTasks as $task){
            if(!in_array($task->getProject(),$projects))
            {
                $allTasks = $this->unsetValue($allTasks, $task);
            }
        }
        return $allTasks;
    }
}
