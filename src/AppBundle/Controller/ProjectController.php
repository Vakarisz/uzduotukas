<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Project;
use AppBundle\Entity\Member;
use AppBundle\Entity\Admins;
use AppBundle\Form\ProjectType;
use AppBundle\Form\Passw_Delete;
use AppBundle\Repository\ProjectRepository;
use AppBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use function Sodium\add;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\HttpFoundation\Response;


class ProjectController extends Controller
{
	/**
     * @Route("/projects", name="projectList")
     */
    public function listProjects(Request $request)
    {
		$userID = $this->getUser()->getid();
        $projects = self::getUserProjects($userID);

        return $this->render('main/projectsList.html.twig',array(
		'projects' => $projects
		));
    }
	/**
     * @Route("/project/create/", name="project_create")
     */
    public function createAction(Request $request)
    {
        $project = new project();

        $form = $this->createForm(ProjectType::class,$project);


        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {

            $name = $form['name']->getData();
            $description = $form['description']->getData();
            $clientName = $form['clientName']->getData();
            $clientContacts = $form['clientContacts']->getData();
            $finishDate = $form['finishDate']->getData();
            $startDate = new\DateTime('now');

            $project->setClientContacts($clientContacts);
            $project->setName($name);
            $project->setDescription($description);
            $project->setClientName($clientName);
            $project->setFinishDate($finishDate);
            $project->setStartDate($startDate);
            $project->setOwnerId($this->getUser()->getid());
			$project->setIsActive(true);

            $em = $this->getDoctrine()->getManager();
            $em->persist($project);
            $em->flush();

            $thisProject = $this->getDoctrine()->getRepository('AppBundle:Project')->findBy(
                array('name' => $name)
            );
            $member = new Member();
            $member->setProject($project);
            $member->setName($this->getUser()->getusername());
            $member->setUser($this->getUser());

            $em2 = $this->getDoctrine()->getManager();
            $em2->persist($member);
            $em2->flush();


            $this->addFlash(
                'success',
                'Project Added'
            );
            return $this->redirectToRoute('projectList');
        }

        return $this->render('main/create.html.twig',array(
            'form' => $form->createView()
        ));
    }
	/**
     * @Route("/project/edit/{id}", name="project_edit")
     */
    public function editAction(Project $project,Request $request)
    {
        $form = $this->createForm(ProjectType::class,$project);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {

            $name = $form['name']->getData();
            $description = $form['description']->getData();
            $clientName = $form['clientName']->getData();
            $clientContacts = $form['clientContacts']->getData();
            $finishDate = $form['finishDate']->getData();
            $startDate = new\DateTime('now');

            $em = $this->getDoctrine()->getManager();

            $project->setClientContacts($clientContacts);
            $project->setName($name);
            $project->setDescription($description);
            $project->setClientName($clientName);
            $project->setFinishDate($finishDate);
            $project->setStartDate($startDate);
            $project->setOwnerId($this->getUser()->getid());

            $em->flush();

            $this->addFlash(
                'success',
                'Project Edited'
            );
            return $this->redirectToRoute('projectList');
        }
        return $this->render('main/edit.html.twig',array(
            'project' => $project,
            'form'    => $form->createView()
        ));
    }
	/**
     * @Route("/project/details/{id}", name="project_details")
     */
    public function indexAction(Project $project)
    {
        $projects = self::getUserProjects($this->getUser()->getId());
        if(in_array($project,$projects,true)) {
            $users = $this->getDoctrine()->getRepository('AppBundle:User')->findAll();
            $members = $project->getMembers();
            $admins = $project->getAdmins();
            foreach ($members as $member)
                $users = $this->unsetValue($users, $member->getProjectUser());
            foreach ($admins as $member)
                $users = $this->unsetValue($users, $member->getProjectAdmin());

            return $this->render('main/details.html.twig', array(
                    'project' => $project,
                    'members' => $members,
                    'users' => $users,
                    'admins' => $admins
                )
            );
        }else{
            $this->addFlash(
                'danger',
                'You dont have that project assigned.'
            );
            return $this->redirectToRoute('projectList');

        }
    }
    /**
     * @Route("/project/quit/{id}", name="project_quit")
     */
    public function quitAction(Project $project,Request $request)
    {
        $user = $this->getUser();

        $form = $this->createForm(Passw_Delete::class);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {

            $password = $form['Password']->getdata();

            $factory = $this->get('security.encoder_factory');
            $encoder = $factory->getEncoder($this->getUser());

            if($encoder->isPasswordValid($this->getUser()->getPassword(),$password,$user->getSalt())){
                $member = $this->getDoctrine()
                    ->getRepository('AppBundle:Member')
                    ->findBy(array(
                        'userId'    => $this->getUser()->getId(),
                        'projectId' => $project->getId()
                    ));

                $em = $this->getDoctrine()->getManager();
                $em->remove($member[0]);
                $em->flush();

                $this->addFlash(
                    'success',
                    'Project Quited'
                );
            }
            else {
                $this->addFlash(
                    'danger',
                    'Incorrect Password'
                );
            }
            return $this->redirectToRoute("projectList");
        }


        return $this->render('main/quit.html.twig',array(
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/project/{id}/add/{userId}/{as}", name="addUserAsMember")
     */
    public function addUserToProject(Project $project,$userId,$as)
    {
            if ($as == "member") {

                $user = $this->getDoctrine()->getRepository('AppBundle:User')->findBy(array(
                    'id' => $userId
                ));
                $member = new Member();
                $member->setProject($project);
                $member->setUser($user[0]);
                $member->setName($user[0]->getusername());


                $em = $this->getDoctrine()->getManager();
                $em->persist($member);
                $em->flush();

                $this->addFlash('success', 'Added as Member!');

            } else if ($as == "admin") {
                $user = $this->getDoctrine()->getRepository('AppBundle:User')->findBy(array(
                    'id' => $userId
                ));
                $admin = new Admins();
                $admin->setProjectId($project->getId());
                $admin->setUserId($userId);
                $admin->setProject($project);
                $admin->setProjectAdmin($user[0]);

                $em = $this->getDoctrine()->getManager();
                $em->persist($admin);
                $em->flush();
                $this->addFlash('success', 'Added as Admin!');
            }

        return $this->redirectToRoute("projectList",array('type' => 'danger'));
    }
    /**
     * @Route("/project/delete/{id}", name="project_delete")
     */
    public function project_delete(Project $project,Request $request)
    {
        $admins = $this->getDoctrine()->getRepository('AppBundle:Admins')->findAll();
        $ids = array();
        foreach($admins as $admin)
            array_push($ids,$admin->getUserId());

        $form = $this->createForm(Passw_Delete::class);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            if ($project->getOwnerId() == $this->getUser()->getId() || in_array($this->getUser()->getId(), $ids)) {
                $password = $form['Password']->getdata();

                $factory = $this->get('security.encoder_factory');
                $encoder = $factory->getEncoder($this->getUser());

                if ($encoder->isPasswordValid($this->getUser()->getPassword(), $password, $this->getUser()->getSalt())) {

                    $members = $this->getDoctrine()->getRepository('AppBundle:Member')->findBy(array(
                        'projectId' => $project->getId()
                    ));

                    $em = $this->getDoctrine()->getManager();
                    foreach($members as $member)
                       $em->remove($member);
                    $em->remove($project);
                    $em->flush();

                    $this->addFlash(
                        'success',
                        'Project Deleted'
                    );
                } else {
                    $this->addFlash(
                        'danger',
                        'Incorrect Password'
                    );
                }
            }else{
                $this->addFlash(
                    'danger',
                    'You are not admin or owner of the project'
                );
            }
            return $this->redirectToRoute("projectList");
        }


        return $this->render('main/quit.html.twig',array(
            'form' => $form->createView()
        ));
    }
    private function unsetValue(array $array, $value, $strict = TRUE)
    {
        if(($key = array_search($value, $array, $strict)) !== FALSE) {
            unset($array[$key]);
        }
        return $array;
    }
    private function getUserProjects($userID){
        $memberOf = $this->getDoctrine()->getRepository('AppBundle:Member')->findBy(
            array('userId' => $userID)
        );
        $projects = array();
        $index = 0;
        foreach($memberOf as $member) {
            $projects[$index] = $member->getProject();
            $index++;
        }
        $memberOf = $this->getDoctrine()->getRepository('AppBundle:Admins')->findBy(
            array('userId' => $userID)
        );
        $index = 0;
        foreach($memberOf as $member) {
            $projects[$index] = $member->getProject();
            $index++;
        }
        return $projects;
    }
}
