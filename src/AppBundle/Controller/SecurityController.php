<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Project;
use AppBundle\Entity\Members;
use AppBundle\Form\LoginType;
use AppBundle\Form\RegistrationType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\User;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\HttpFoundation\Response;

class SecurityController extends Controller
{
	/**
	 * @Route("/", name="index")
	 */
	public function homeAction(Request $request)
	{
		$projects = $this->getDoctrine()->getRepository('AppBundle:Project')
			->findAll();
		$securityContext = $this->container->get('security.authorization_checker');
		if($securityContext->isGranted('IS_AUTHENTICATED_FULLY') ){
			return $this->render('main/index.html.twig', array(
				'projects' => $projects
			));
		}else {
			$authenticationUtils = $this->get('security.authentication_utils');
			$error = $authenticationUtils->getLastAuthenticationError();
			$reg_form = $this->createForm(RegistrationType::class);
			$login_form = $this->createForm(LoginType::class);
			return $this->render(
				'index.html.twig',
				array('reg_form' => $reg_form->createView(),'login_form' => $login_form->createView(), 'error' => $error)
			);
		}
	}

	/**
	 * @Route("/registrationProcess", name="process_registration")
	 */
	public function processRegistration(Request $request)
	{
		$user = new User();
		$reg_form = $this->createForm(RegistrationType::class,$user);
		// 2) handle the submit (will only happen on POST)
		$reg_form->handleRequest($request);
		if ($reg_form->isSubmitted() && $reg_form->isValid()) {
			// 3) Encode the password (you could also do this via Doctrine listener)
			$password = $this->get('security.password_encoder')
				->encodePassword($user, $user->getPassword());
			$user->setPassword($password);

			// 4) save the User!
			$em = $this->getDoctrine()->getManager();
			$em->persist($user);
			$em->flush();

			// ... do any other work - like sending them an email, etc
			// maybe set a "flash" success message for the user
			$this->addFlash(
				'notice',
				'User successfully created!'
			);
			return $this->redirectToRoute('index');
		} else {
			$errors = $reg_form->getErrors(true,false);
			$this->addFlash(
				'notice',
				(string)$errors
			);
			return $this->redirectToRoute("index");
		}
	}
}
