"Uzduotukas" is a simple-to-use online task managing system. It's newer been so easy to share the workload!
It's feautures include:
• Creating and managing projects
• Adding and managing project members
• Creating and editing tasks for each project
• Picking or quiting your tasks and inspecting who is working on certain task

Required technologies to run: PHP ~7, MySql 5.7.11
To install "Uzduotukas" follow these steps:
1. Install composer on your machine.
2. Install symfony project by opening console and typing command:  composer create-project symfony/framework-standard-edition Uzduotukas
3. Pull Uzduotukas from repository on top of recently created symfony project.
4. Setup database server and create new database for "Uzduotukas"
5. Configure database connection by editing Uzduotukas/app/confi/parameters.yml
6. Update database tables by running console comand "php bin/console doctrine:schema:update --force"
7. Register and use Uzduotukas task managing system!